import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomEditText extends StatelessWidget {
  CustomEditText(
      {required this.controller, this.horizontalPadding = 20.0,
       this.label = "",
      this.hint = "", this.width = double.infinity, this.inputType = TextInputType.text,
      this.textCapitalization = TextCapitalization.words,
      this.textAlign = TextAlign.start, this.isPasswordField = false,
      this.maxLine = 1, this.maxLength=300, this.enabled = true,
      }
      );

  final double horizontalPadding;
  final TextEditingController controller;
  final String label;

  final String hint;
  final double width;

  final int maxLine;
  final int maxLength;

  final TextAlign textAlign;
  final bool isPasswordField;
  final TextInputType inputType;
  final TextCapitalization textCapitalization;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15.0),
      width: width,
      decoration: BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.all(const Radius.circular(15.0),)
      ),
      child: TextFormField(
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        enabled: enabled,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        cursorColor: Colors.black,
        decoration: new InputDecoration(
          alignLabelWithHint: true,
          fillColor: Colors.black12,
          counterText: "",
          border: new OutlineInputBorder(
              borderRadius: const BorderRadius.all(const Radius.circular(15.0)),
              borderSide: new BorderSide(color: Colors.black12)),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(
                color: Colors.black12, width: 1.0),
            borderRadius: BorderRadius.circular(15.0),
          ),
          contentPadding: EdgeInsets.symmetric(
              vertical: 15, horizontal: horizontalPadding),
          // labelText: label,
          hintText: hint,
          hintStyle: TextStyle(
            fontSize: 16.0,
            color: Colors.white,
            fontFamily: 'releway-bold',
          ),
          // labelStyle: TextStyle(
          //     fontSize:14.0,color: DARK_TEXT_COLOR,
          //
          // )
        ),
        style: TextStyle(
          color: Colors.white,
          fontSize: 16.0,
          fontFamily: 'releway-bold',
        ),
        maxLines: maxLine,
        maxLength: maxLength,
        controller: controller,
        keyboardType: inputType,
        textCapitalization: textCapitalization,
        textAlign: textAlign,
        autocorrect: false,
        textInputAction: TextInputAction.next,
        // validator: fieldValidator,
        obscureText: isPasswordField,
      ),
    );
  }
}