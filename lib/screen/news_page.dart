
import 'dart:convert' as convert;

import 'package:api_news/model/news_model.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({Key? key}) : super(key: key);

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {

  bool isNewsSelected = true;
  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                  child: SizedBox(width: double.infinity,height: MediaQuery.of(context).size.height,child: isNewsSelected ? _newsView() : _favsView()),
              ),

              _toggleView()
            ],
          ),
        ),
      ),
    );
  }

  Widget _toggleView() {
    return Row(
      children: [
        InkWell(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(5)),
                color: isNewsSelected ? Colors.blue : Colors.white
            ),
            height: 50,
            width: MediaQuery.of(context).size.width*0.45,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.format_list_bulleted,
                  color: isNewsSelected ? Colors.white : Colors.red,
                ),

                SizedBox(width: 10,),
                Text("News", style: TextStyle(
                    fontSize: 20,
                  color: isNewsSelected ? Colors.white : Colors.black,
                ),)
              ],
            ),
          ),
          onTap: (){
            isNewsSelected = true;
            setState(() {

            });
          },
        ),

        SizedBox(width: 10,),

        InkWell(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(5)),
                color: !isNewsSelected ? Colors.blue : Colors.white
            ),
            height: 50,
            width: MediaQuery.of(context).size.width*0.45,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.favorite,
                  color: !isNewsSelected ? Colors.white : Colors.red,
                ),

                SizedBox(width: 10,),
                Text("Favs", style: TextStyle(
                  fontSize: 20,
                  color: !isNewsSelected ? Colors.white : Colors.black,
                ),)
              ],
            ),
          ),
          onTap: (){
            isNewsSelected = false;
            setState(() {

            });
          },
        ),
      ],
    );
  }

  Widget _newsView() {
    return FutureBuilder(
      future: _getNews(),
        builder: (context, snapshot){
        debugPrint(snapshot.data.toString());
        if(snapshot.hasData){
          List<NewsData> newsdata = snapshot.data as List<NewsData>;
          return ListView.builder(
              itemCount: newsdata.length,
              itemBuilder: (BuildContext context,int index){
                return Card(
                  margin: EdgeInsets.all(5.0),
                  elevation: 5,
                  shadowColor: Colors.grey,
                  child: Row(
                      children: [
                        Container(
                          width: 50.0,
                          child: Icon(
                            Icons.favorite,
                            color: isNewsSelected ? Colors.grey : Colors.red,
                          ),
                        ),
                        Expanded(
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(5.0),
                                  child: Text(newsdata[index].title.toString(),overflow: TextOverflow.ellipsis,maxLines: 2,style: TextStyle(
                                    fontWeight: FontWeight.bold
                                  ),),
                                ),
                                Padding(padding: EdgeInsets.all(5.0),
                                  child: Text(newsdata[index].summary.toString(),overflow: TextOverflow.ellipsis,maxLines: 2),
                                ),
                                Text(newsdata[index].published.toString(),overflow: TextOverflow.ellipsis,style: TextStyle(
                                  color: Colors.black12
                                ),),
                              ],
                            ),
                          ),
                        )
                      ],/*Text(newsdata[index].title.toString(),
                        style: TextStyle(
                            color: Colors.green,fontSize: 15),),*/
                  ),
                );
              }
          );
        }else{
          return Container(width: 20, height: 20,child: CircularProgressIndicator(strokeWidth: 1,));
        }

    });
  }

  Widget _favsView() {
    return  Container(
      width: 50,
      height: 50,
      color: Colors.grey,
    );
  }

  Future<List<NewsData>?> _getNews() async{
    var url = Uri.parse('https://api.first.org/data/v1/news');

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body) as Map<String, dynamic>;

      var newsList = NewsModel.fromJson(jsonResponse);
      debugPrint(newsList.toString());

      return newsList.newsList;
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }

    return null;
  }
}
