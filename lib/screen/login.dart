import 'package:api_news/CustomEditText.dart';
import 'package:flutter/material.dart';
class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    TextEditingController emailcon = TextEditingController(),passcont = TextEditingController();
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/download.jpeg"),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 30.0,bottom: 150.0),
                    child: Text('Welcome!!',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30.0,
                      color: Colors.white
                    ),
                    )),
                Container(
                  height: height*0.6,
                  padding: EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(25.0),topRight: Radius.circular(25.0))
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text('Sign In',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25.0,
                            color: Colors.black
                        ),
                      ),
                      SizedBox(height: 20.0,),
                      CustomEditText(controller: emailcon,hint: 'Email:',),
                      CustomEditText(controller: passcont,hint: "Password:",isPasswordField: true,),
                      Align(alignment: Alignment.centerRight,child: Text('Forgot password?'),),
                      SizedBox(height: 20.0,),
                      SizedBox(
                        height: 50.0,
                        width:150.0,
                        child: ElevatedButton(
                          onPressed: () {Navigator.pushNamed(context, '/newspage');},
                          child: Text('Login'),
                          style: ElevatedButton.styleFrom(shape: StadiumBorder()),
                        ),
                      ),
                      SizedBox(height: 20.0,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(child: Divider()),
                          Text('Or Sign In With'),
                          Expanded(child: Divider())
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(image: AssetImage('images/g.png'),width: 50.0,height: 50.0,),
                          SizedBox(width: 20.0,),
                          Image(image: AssetImage('images/fb.png'),width: 50.0,height: 50.0,)
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Don't have an account?"),
                          InkWell(
                            onTap: (){
                              Navigator.pushNamed(context, '/signup');
                            },
                              child: Text('  Sign Up',style: TextStyle(
                                color: Colors.red
                              ),)
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
