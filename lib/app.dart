import 'package:api_news/screen/login.dart';
import 'package:api_news/screen/news_page.dart';
import 'package:api_news/screen/signup.dart';
import 'package:flutter/material.dart';
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    title:'News App',
    routes: {
      '/login': (BuildContext context) => Login(),
      '/signup': (BuildContext context) => SignUp(),
      '/newspage': (BuildContext context) => NewsPage(),
    },
    // darkTheme: themeData(ThemeConfig.darkTheme),
    home: Login()
  );
}
